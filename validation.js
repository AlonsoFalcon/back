/**
 * Module to validate if the request body
 * corresponds to the user scheme defined
 */

// Validation
const Joi = require("@hapi/joi");

const schemaRegister = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  birthDate: Joi.string().required(),
  email: Joi.string().required(),
  password: Joi.string().min(8).required(),
});

const schemaLogin = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().min(8).required(),
});

const registerValidation = (data) => {
  return schemaRegister.validate(data);
};

const loginValidation = (data) => {
  return schemaLogin.validate(data);
};
module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
