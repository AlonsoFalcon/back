const express = require("express");
const app = express();

// Importing Routes
const authRoute = require("./routes/auth.js");

// General Middleware
app.use(express.json());

// Route Middlewares
app.use("/api/users", authRoute);

app.listen(3000, () => console.log("Server running on port 3000"));
