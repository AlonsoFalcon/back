const router = require("express").Router();
const dotenv = require("dotenv");
const requestJson = require("request-json");
const bcrypt = require("bcryptjs");
const { registerValidation, loginValidation } = require("../validation");

const User = require("../model/User");
const e = require("express");
dotenv.config();

let urlUsers = `https://api.mlab.com/api/1/databases/${process.env.DB_NAME}/collections/${process.env.USERS_COLLECTION}?apiKey=${process.env.API_KEY}`;
const clienteMLab = requestJson.createClient(urlUsers);
const saltRounds = 10;

router.post("/register", (req, res) => {
// Validation before create a user
const { error } = registerValidation(req.body);

if (error) {
    res.status(400).send(error.details[0].message);
  } else {
    const user = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      birthDate: req.body.birthDate,
      email: req.body.email,
      password: req.body.password,
    });

    let urlCheckForEmail = `https://api.mlab.com/api/1/databases/${process.env.DB_NAME}/collections/${process.env.USERS_COLLECTION}?q={"email":{"$eq":"${user.email}"}}&c=true&apiKey=${process.env.API_KEY}`;
    const clientCheckEmail = requestJson.createClient(urlCheckForEmail);

    clientCheckEmail.get("", (err, resM, body) => {
      if (err) {
        return res.status(500).send(err);
      } else {
        if (parseInt(resM.body) > 0) {
          res.status(409).send("The email already exists");
        } else {
          // Hashing the Password
          bcrypt.genSalt(saltRounds, (err, salt) => {
            if (err) {
              res.status(500).send("Algo esta pasando, intenta de nuevo");
            }
            bcrypt.hash(user.password, salt, (err, hash) => {
              if (err) {
                res.status(500).send("Algo esta pasando, intenta de nuevo");
              } else {
                user.password = hash;
                //This method is a Promise
                clienteMLab.post("", user, (err, resM, body) => {
                  if (err) {
                    res.status(400).send(err);
                  }
                  res.status(201).send(body.id);
                });
              }
            });
          });
        }
       }
     });
  }
});

router.post("/login", (req, res) => {
  const { error } = loginValidation(req.body);
  if (error) {
    res.status(400).send(error.body[0].message);
  } else {
    // consult if the email exists
    let urlCheckForEmail = `https://api.mlab.com/api/1/databases/${process.env.DB_NAME}/collections/${process.env.USERS_COLLECTION}?q={"email":{"$eq":"${req.body.email}"}}&apiKey=${process.env.API_KEY}`;
    const clientCheckEmail = requestJson.createClient(urlCheckForEmail);
    clientCheckEmail.get("", (err, resM, body) => {
      if (err) {
        return res.status(500).send(err);
      } else {
        if (!body) {
          res.status(404).send("El usuario no existe");
        } else {
          // the email exists, check for the password
          bcrypt.compare(req.body.password, body[0].password, (err, result) => {
            if (err) {
              res.status(500).send("Algo esta mal, intenta de nuevo");
            } else {
              if (result) {
                const token = jwt.sign(
                   { _id: body[0]._id },
                   process.env.TOKEN_SECRET
                );
                res.header("auth-token", token).status(200).send(token);
              } else {
                res.status(400).send("Contraseña incorrecta");
              }
            }
          });
        }
      }
    });
  }
});

module.exports = router;
